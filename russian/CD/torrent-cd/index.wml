#use wml::debian::cdimage title="Загрузка образов Debian для USB/CD/DVD с помощью BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e" maintainer="Lev Lamberov"

<p><a href="https://ru.wikipedia.org/wiki/BitTorrent">BitTorrent</a>&nbsp;&mdash;
это пиринговая система, оптимизированная для большого числа параллельных
загрузок. Её использование снижает нагрузки на наши серверы, поскольку
клиенты BitTorrent по мере загрузки передают части файлов остальным, более
равномерно распределяя таким образом нагрузку по сети и ускоряя загрузку.
</p>
<div class="tip">
<p><strong>Первый</strong> USB/CD/DVD диск содержит все файлы, необходимые для
установки стандартной системы Debian.<br />
</p>
</div>
<p>
Чтобы загрузить образы USB/CD/DVD Debian этим способом, вам понадобится клиент
BitTorrent. Дистрибутив Debian содержит
<a href="https://packages.debian.org/aria2">aria2</a>,
<a href="https://packages.debian.org/transmission">transmission</a> или
<a href="https://packages.debian.org/ktorrent">KTorrent</a>.
Другие операционные системы (типа Windows и macOS) поддерживаются следующими клиентами: <a
href="https://www.qbittorrent.org/download">qBittorrent</a> и <a
href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>

<h3>Официальные файлы torrent для <q>стабильного</q> выпуска</h3>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>Обязательно просмотрите документацию перед установкой.
<strong>Если вы хотите быстрее начать установку</strong>, прочитайте наше
<a href="$(HOME)/releases/stable/amd64/apa">Руководство по установке</a>, быстрое
введение в процесс установки. Другая полезная информация:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Руководство по установке</a>,
    детальные инструкции по установке</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Документация по
    Debian-Installer</a>, включает в себя FAQ с общими вопросами и ответами</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    Errata</a>, список известных проблем в программе установки</li>
</ul>

#<h3>Официальные файлы torrent для <q>тестируемого</q> дистрибутива</h3>
#
#<ul>
#
#  <li><strong>CD</strong>:<br />
#  <full-cd-torrent>
#  </li>
#
#<ul>
#  <li><strong>DVD</strong>:<br />
#  <full-dvd-torrent>
#  </li>
#
#</ul>

<p>
Если у вас есть возможность, после завершения загрузки не завершайте работу
вашего клиента чтобы помочь остальным загрузить образы быстрее!
</p>

#use wml::debian::template title="Debian BTS - reporting bugs" NOHEADER=yes NOCOPYRIGHT=true
#use wml::debian::translation-check translation="d3f8e5a80e17ede3b0af302c395ff45c59cdd4a5"


<h1>reportbug를 써서 데비안 버그를 보고하는 방법</h1>
<a name="reportbug"></a>

<p>데비안에서 버그를 보고할 때 우리는
<code><a href="https://packages.debian.org/stable/utils/reportbug">reportbug</a></code>
프로그램을 사용하길 강력히 권합니다.</p>

<p>
reportbug는 대부분의 시스템에 기본으로 설치됩니다. 없다면, 패키지 관리 도구를
사용해 설치할 수 있습니다.
</p>

<p>
reportbug는 메뉴의 시스템 섹션 또는 명령행에서 <code>reportbug</code>를
실행하여 시작할 수 있습니다.
</p>

<p>여기에서 버그 보고 절차를 단계적으로 안내합니다.</p>

<p>
reportbug의 대화형 프롬프트에서 해결할 수 없는 질문이 있으면, 아래 문서의
나머지 부분을 참조하거나 <a href="mailto:debian-user@lists.debian.org">데비안
사용자 메일링 리스트</a>에 문의할 수 있습니다.
</p>


<h1>이메일을 사용해 데비안 버그를 보고하는 법 (그리고 고급 reportbug
사용법)</h1>

<h2>버그 보고를 보내기 <strong>전에</strong> 주의할 중요한 사항</h2>

<a name="whatpackage"></a>
<h3>버그 보고가 무슨 패키지에 해당되나?</h3>
<p>
버그 보고서가 어떤 패키지에 대해 제출되어야 하는지 알아야 합니다. 이 정보를
찾는 방법에 대한 자세한 내용은 <a href="#findpkgver">이 예시</a>를 보십시오.
(이 정보를 사용하여 <a href="#filedalready">버그 보고서가 이미
제출되었는지</a> 확인합니다.)
</p>

<p>버그 보고를 어느 패키지에 할 지 모르면,
이메일을 <a href="mailto:debian-user@lists.debian.org">데비안 사용자 메일링
리스트</a>에 보내서 조언을 요청하세요.</p>

<p>만약 여러분의 문제가 단지 하나의 패키지가 아니라 일부 일반적인 데비안
서비스와 관련이 있다면, 대신 우리에게 메시지를 전달하는 데 사용할 수 있는 몇
개의 <a href="pseudo-packages">유사 패키지</a>가 있습니다. 아니면 <a
href="../MailingLists/">메일링 리스트</a>도 있습니다.</p>

<a name="filedalready"></a>
<h3>버그 보고가 이미 제출되었나?</h3>
<p>보고서를 제출하기 전에 이미 버그 보고가 되어 있는지 확인해야 합니다. 특정
패키지에 보고된 버그를
<a href="./#pkgreport">버그 검색 패키지 옵션</a>을 사용해 찾아볼 수 있습니다.
이미 기존의 버그 리포트 #<var>&lt;번호&gt;</var>가 있으면, 새로운 버그를 보고하지 말고
<var>&lt;번호&gt;</var>@bugs.debian.org 주소로 메일을 보내 댓글을 달 수
있습니다.</p>

<h3>여러 버그에 대해 여러 보고서 보내기</h3>
<p>여러 개의 관계 없는 버그를 하나의 보고서에 보고하지 마십시오. 특히 다른
패키지의 여러 버그를 한 번에 보고하지 마십시오.</p>

<h3>버그를 업스트림에 보내지 마세요</h3>
<p>만약 데비안에서 버그를 제출한다면, 그 버그가 데비안에만 존재할 가능성이 있기
때문에, 업스트림 소프트웨어 관리자에게 복사본을 보내지 마세요. 필요한 경우
패키지의 관리자가 버그를 업스트림에 전달할 것입니다.</p>

<h2>이메일을 통해 버그 보고 보내기</h2>

<p><a href="mailto:submit@bugs.debian.org"><code>submit@bugs.debian.org</code></a>
주소에 아래의 특별한 형식의 이메일을 보내 데비안의 버그를 보고할 수 있습니다.
<code>reportbug</code>를 사용하면 (<a href="#reportbug">위 내용을
보십시오</a>) 적절하게 이메일 형식을 맞춰 줍니다. 그러니 reportbug를
사용하세요!</p>

<h3>헤더</h3>
<p>이메일과 마찬가지로, 내용을 설명하는 <code>Subject</code>(제목) 라인이
들어가야 합니다. 제목은 버그 추적 시스템의 최초 버그 제목으로 사용되므로,
유익한 정보가 들어가도록 제목을 정하십시오!</p>

<p>버그 보고서의 복사본을 다른 수신자에게 (메일링 리스트 등) 보내려면,
일반적인 이메일 주소를 사용하면 안 되고, <a href="#xcc">아래에 설명한 다른
방법을</a> 사용하십시오.</p>

<h3><a name="pseudoheader">유사 헤더</a></h3>
<p>버그 리포트의 첫 부분은 유사 헤더로 버그 리포트가 적용될 패키지와 버전과
같은 정보가 들어 있습니다. 메시지 본문의 첫 줄에 유사 헤더 줄이 들어가야
합니다. 다음과 같습니다:</p>

<pre>
Package: &lt;패키지이름&gt;
</pre>

<p>위의 <code>&lt;패키지이름&gt;</code> 대신에 버그가
있는 <a href="#whatpackage">패키지의 이름</a>을 사용하십시오.</p>

<p>메시지의 둘째 줄은 다음과 같아야 합니다:</p>

<pre>
Version: &lt;패키지버전&gt;
</pre>

<p>위의 <code>&lt;패키지버전&gt;</code> 대신에 패키지의 버전을 써야 합니다.
버전 자체 외에 다른 문구를 쓰지 마세요. 버그 시스템은 이 필드에 따라 어느
릴리스가 해당 버그에 영향을 받는지 판단하기 때문에 버전만 써야 합니다.</p>

<p>올바른 <code>Package</code> 라인을 유사 헤더에 써야 버그 추적 시스템에서
해당 메시지를 패키지 관리자에게 전달합니다. <a href="#findpkgver">이
예제에서</a> 어떻게 이 정보를 찾을 수 있는지 볼 수 있습니다.</p>

<p>기타 사용할 수 있는 유사 헤더에
대해서는, <a href="#additionalpseudoheaders">추가 유사 헤더를</a>
참고하십시오.</p>

<h3>보고서의 본문</h3>
<p>보고서에 다음을 쓰십시오:</p>

<ul>
<li>표시되거나 기록에 남은 에러 메시지의 <em>정확</em>하고 <em>완전</em>한
텍스트. 매우 중요합니다!</li>
<li>문제를 경험할 때 정확히 무엇을 입력하고 무엇을 했는지.</li>
<li>잘못된 동작에 대한 설명: 정확히 어떤 동작을 기대했는지와, 무엇을 실제
관찰했는지. 이를 설명하려면 예시 세션 기록이 한 가지 좋은 방법입니다.</li>
<li>제안하는 수정 방법, 또는 패치 (있다면).</li>
<li>문제가 있는 프로그램의 자세한 설정. 설정 파일의 전체 텍스트를 포함하십시오.</li>
<li>버그가 있는 패키지가 의존하는 모든 패키지의 버전.</li>
<li>사용하는 커널의 버전 (<code>uname -a</code> 명령), 사용하는 C library
(<code>ls -l /lib/*/libc.so.6</code> 또는
<code>apt show libc6 | grep ^Version</code> 명령), 기타 필요한 경우 데비안
시스템의 자세한 정보. 예를 들어, Perl 스크립트에 문제가 있으면, 'perl'
바이너리의 버전을 포함합니다 (<code>perl -v</code> 또는 <code>dpkg -s perl |
grep ^Version:</code> 명령).</li>
<li>필요한 경우 시스템 하드웨어의 정보. 장치 드라이버와 관련된 문제를
보고한다면 시스템의 <em>모든</em> 하드웨어 목록을 포함하십시오. 때때로 문제는
IRQ와 I/O 주소 충돌로 발생하기 때문에 필요합니다.</li>
<li><a href="https://packages.debian.org/stable/utils/reportbug">reportbug</a>가
설치되어 있으면 <code>reportbug --template -T none -s none -S normal -b
 --list-cc none -q &lt;패키지&gt;</code> 명령의 결과도 유용합니다. 여기에는
메인테이너가 스크립트의 결과와 버전 정보가 들어갑니다.</li>
</ul>

<p>관련되어 보이는 모든 상세 정보를 포함합니다. 지나치게 많은 정보를 포함해서
보고서가 너무 길어질 위험은 별로 없습니다. 길지 않은 내용이면, 문제를
재현하는데 사용한 파일도 모두 포함해 주십시오. (내용이 길다면, 가능하면
공개적으로 접근할 수 있는 웹사이트에서 사용할 수 있게 해 주십시오.)</p>

<p>개발자가 문제를 해결하는데 도움을 주고 싶다면,
<a href="https://www.chiark.greenend.org.uk/~sgtatham/bugs.html">
효과적으로 버그를 보고하는 법</a>을 읽어 보십시오.</p>


<h2><a name="example">버그 보고 예시</a></h2>

<p>헤더와 유사 헤더를 포함한 버그 보고서는 다음과 같습니다:</p>

<pre>
  To: submit@bugs.debian.org
  From: diligent@testing.linux.org
  Subject: Hello says `goodbye'

  Package: hello
  Version: 1.3-16

  When I invoke `hello' without arguments from an ordinary shell
  prompt it prints `goodbye', rather than the expected `hello, world'.
  Here is a transcript:

  $ hello
  goodbye
  $ /usr/bin/hello
  goodbye
  $

  I suggest that the output string, in hello.c, be corrected.

  I am using Debian GNU/Linux 2.2, kernel 2.2.17-pre-patch-13
  and libc6 2.1.3-10.
</pre>


<h2><a name="xcc">버그 보고 사본을 다른 주소에 보내기</a></h2>

<p>때때로 버그 보고서의 복사본을 (항상 보내게
되는) <code>debian-bugs-dist</code>나 패키지 관리자 외에 다른 곳에 보내야 할
경우가 있습니다.</p>

<p>버그 리포트에 다른 곳의 메일 주소를 참조에 넣어서 보낼 수도 있지만, 그렇게
하면 그 다른 복사본에는 <code>Reply-To</code> 필드나 <code>Subject</code> 줄에
들어가는 버그 보고서 번호가 들어가지 않습니다. 수신자가 답장을 하면 아마도
헤더에 들어 있는
<code>submit@bugs.debian.org</code> 주소를 유지할 것이고, 답장 메시지를 새로운
버그 리포트로 보내게 됩니다. 이런 일이 발생하면 중복된 버그 보고서를 보내게
됩니다.</p>

<p><em>올바른</em> 방법은 <code>X-Debbugs-CC</code> 유사 헤더를 사용하는
것입니다. 이 줄을 메시지의 유사 헤더에 다음과 같이 추가합니다:
<pre>
 X-Debbugs-CC: other-list@cosmic.edu
</pre>
<p>이렇게 하면 버그 추적 시스템은 버그 보고서의 복사본을
<code>debian-bugs-dist</code> 뿐만 아니라 <code>X-Debbugs-CC</code> 줄에 들어
있는 주소에 보냅니다.</p>

<p>두 개 이상의 메일 주소에 복사본을 보내고 싶으면, 메일 주소를 쉼표로 구분해
하나의 <code>X-Debbugs-CC</code> 줄에 씁니다.</p>

<p>다른 버그 보고의 주소에 이러한 복사본을 보내지 마십시오. 그러한 복사본은
메일 루프 방지 검사에 의해 걸릴 것입니다. 이러한 목적으로는
<code>X-Debbugs-CC</code> 사용은 상대적으로 의미가 적습니다. 시스템에서 추가한
버그 번호는 새로운 버그 번호로 대체되어 버리기 때문입니다. 이 경우
일반 <code>CC</code>(참조) 헤더를 사용하십시오.</p>

<p>이 기능은 메일링의 <code>quiet</code> 기능과 결합해 유용하게 사용됩니다
&mdash; 아래를 참조하십시오.</p>

<a name="additionalpseudoheaders"></a>
<h1>추가 유사 헤더</h1>

<h2><a name="severities">심각도</a></h2>

<p>어떤 버그가 특히 심각한 버그인 경우, 아니면 단순한 기능 요청인 경우, 버그를
보고할 때 버그의 심각도를 지정할 수 있습니다. 물론 지정하지 않아도 되고,
지정하지 않으면 (또는 잘못된 심각도를 지정하면) 패키지 관리자가 적절한
심각도를 지정할 것입니다.</p>

<p>심각도를 지정하려면, 다음과 같은 줄을
<a href="#pseudoheader">유사 헤더</a>에 씁니다:</p>

<pre>
Severity: &lt;<var>심각도</var>&gt;
</pre>

<p>Replace &lt;<var>심각도</var>&gt; 자리에
<a href="Developer#severities">고급 문서</a>에 설명된 사용할 수 있는 심각도 중
하나를 쓰십시오.</p>

<h2><a name="tags">태그 지정하기</a></h2>

<p>버그를 보고할 때 태그를 지정할 수 있습니다. 예를 들어, 버그 보고에 패치를
포함할 경우, <code>patch</code> 태그를 포함합니다. 태그는 꼭 붙이지 않아도
되지만, 개발자가 여러분의 보고서에 적절한 태그를 지정할 것입니다.</p>

<p>태그를 지정하려면, 다음과 같이 <a href="#pseudoheader">유사 헤더</a> 한
줄을 넣습니다:</p>

<pre>
Tags: &lt;<var>태그</var>&gt;
</pre>

<p>&lt;<var>태그</var>&gt; 대신 사용할 수 있는 태그 하나 이상을
<a href="Developer#tags">고급 문서</a>에 설명된 것처럼 넣습니다.
태그가 여러개이면 쉼표나 공백으로 구분합니다.</p>

<pre>
User: &lt;<var>사용자이름</var>&gt;
Usertags: &lt;<var>사용자태그</var>&gt;
</pre>

<p>&lt;<var>사용자태그</var>&gt; 대신 사용자태그 하나 이상을 넣습니다. 태그가
여러개이면 쉼표나 공백으로 구분합니다. &lt;<var>사용자이름</var>&gt;을
지정하면, 해당 사용자의 태그를 설정합니다. 사용자 이름을 지정하지 않으면, 보낸
사람의 이메일 주소를 사용자 이름으로 사용합니다.</p>

<p>You can set usertags for multiple users at bug submission time by
including multiple User pseudo-headers; each Usertags pseudo-header
sets the usertags for the preceding User pseudo-header. This is especially
useful for setting usertags for a team with multiple users, setting
usertags for multiple teams, or setting the
<a href="https://wiki.debian.org/Teams/Debbugs/ArchitectureTags">architecture usertags</a>
for bugs affecting multiple architectures.
</p>

<pre>
User: &lt;<var>first-username</var>&gt;
Usertags: &lt;<var>first-username usertags</var>&gt;
User: &lt;<var>second-username</var>&gt;
Usertags: &lt;<var>second-username usertags</var>&gt;
</pre>

<h2>전달 설정하기</h2>
<pre>
Forwarded: <var>foo@example.com</var>
</pre>

<p>
위와 같이 하면 새로 보고한 버그를 foo@example.com에 전달한 것으로 표시합니다.
자세한 정보는 개발자 문서의 <a href="Developer#forward">버그 보고를 전달했다고
기록하기</a>를 보십시오.
</p>

<h2>소유권 주장하기</h2>
<pre>
Owner: <var>foo@example.com</var>
</pre>

<p>
위와 같이 하면 이제 foo@example.com 주소를 이 버그 해결의 책임자로 지정합니다.
자세한 정보는 개발자 문서의 <a href="Developer#owner">버그 소유권 바꾸기</a>를
보십시오.
</p>

<h2>소스 패키지</h2>
<pre>
Source: <var>foopackage</var>
</pre>

<p>
foopackage 소스 패키지에 들어있는 버그에 대해 <code>Package:</code> 대신 씁니다.
대부분 패키지의 대부분 버그의 경우 이 옵션을 쓰지 않을 것입니다.
</p>

<h2><a name="control">조작 명령</a></h2>
<pre>
Control: <var>조작 명령</var>
</pre>

<p>
위와 같이 <code>control@bugs.debian.org</code> 주소로 보낼 모든 명령을
<code>submit@bugs.debian.org</code>이나 <code>nnn@bugs.debian.org</code>
주소에서 쓸 수 있습니다. -1이라고 쓰면 현재 버그를 (submit@에 보내는 메일에서
새로 만들 버그, 또는 nnn@에 보내는 메일의 해당 버그) 의미합니다. 조작 명령에
대한 자세한 정보는 <a href="server-control">서버 조작 문서</a>를 보십시오.</p>

<p>예를 들어, 메시지에서 다음 유사헤더를 <code>12345@bugs.debian.org</code>
주소로 보내면:</p>

<pre>
Control: retitle -1 this is the title
Control: severity -1 normal
Control: summary -1 0
Control: forwarded -1 https://bugs.debian.org/nnn
</pre>

<p>그러면 12345 버그의 제목을 바꾸고, 심각도를 바꾸고, 요약문을 설정하고,
전달한 것으로 표시합니다.</p>



<h2>X-Debbugs- 헤더</h2>
<p>마지막으로, 여러분의 <acronym title="Mail User Agent" lang="en">MUA</acronym>에서
헤더 편집을 허용하지 않는 경우, 여러가지 <code>X-Debbugs-</code> 헤더를
<a href="#pseudoheader">유사헤더</a>에서 지정할 수 있습니다.</p>


<h1>추가 정보</h1>

<h2>다른 제출 주소 (가벼운 버그 또는 대량 버그 보고)</h2>

<p>만약 버그 보고서가 가벼운 버그에 대한 것이거나, 문서 상의 오타, 또는 단순한
빌드 문제인 경우, 심각도를 조정하고 <code>maintonly@bugs.debian.org</code>
주소로 (<code>submit@bugs.debian.org</code> 주소 대신) 보내십시오.
<code>maintonly</code> 주소를 쓰면 버그 보고서를 패키지 관리자에게만 보내고,
BTS 메일링 리스트에는 전달하지 않습니다.</p>

<p>여러 보고서를 한번에 보내는 경우,
반드시 <code>maintonly@bugs.debian.org</code> 주소를 써야 합니다. 그래야
지나치게 반복적인 트래픽이 BTS 메일링 리스트에 발생하지 않습니다. 비슷한
다수의 버그를 제출하기 전에, 요약문을 <code>debian-bugs-dist</code> 메일링에
보내면 좋습니다.</p>

<p>이미 관리자에게 보낸 버그를 버그 추적 시스템에
보고하려면 <code>quiet@bugs.debian.org</code> 주소를 사용하면
됩니다. <code>quiet@bugs.debian.org</code> 주소로 보낸 버그는 어디에도 전달되지
않고 시스템에 쌓이기만 합니다.</p>

<p>다른 버그 제출 주소를 사용할 때, 버그 추적 시스템은 모든 전달된 메시지에
<code>Reply-To</code> 헤더를 설정해, 답장도 기본값으로 최초 보고서와 같은
방식으로 처리되도록 합니다. 즉, <code>maintonly</code>에 대한 답장은
<var>nnn</var><code>-maintonly@bugs.debian.org</code> 주소로
(<var>nnn</var><code>@bugs.debian.org</code> 주소가 아님) 보냅니다.
물론 수동으로 바꿀 수는 있습니다.</p>


<h2>확인</h2>

<p>보통, 새로운 버그를 제출하거나 기존 버그에 새로운 정보를 보내면, 버그 추적
시스템에서 이메일로 확인 메시지를 보냅니다. 확인 메일을 받고 싶지 않으려면,
이메일에 <code>X-Debbugs-No-Ack</code> 헤더를 (헤더의 값은 중요하지 않습니다)
넣으십시오. 이 헤더가 들어 있는 새로운 버그를 보낸다면, 웹 인터페이스에서 직접 버그
번호를 확인해야 합니다.</p>

<p>단, <code>control@bugs.debian.org</code> 메일 서버에서 보내는 확인 메시지는
이 헤더로 막을 수 없습니다. 여기서는 확인 메시지에 에러 메시지가 들어 있어서,
확인하고 대응해야 하기 때문입니다.</p>

<h2>스팸 차단 및 잃어버린 메일</h2>

<p>버그 추적 시스템에서는 스팸이 BTS를 통과하지 못하도록 상당히 광범위한
규칙을 적용합니다. 스팸 오탐지를 최소화하려고 노력하지만, 오탐지는 발생하게
되어 있습니다. 메일이 스팸으로 오탐지된다고
의심되면, <code>owner@bugs.debian.org</code>에 도움을 요청하십시오. 메일이
BTS를 통과하지 못하는 또 다른 흔한 이유는 procmail의 FROM_DAEMON에 일치하는
경우입니다. (<code>mail@foobar.com</code>와 같은 메일 주소가 여기에
해당합니다.) 여러분의 메일 주소가 FROM_DAEMON에 일치한다고
의심된다면, <a href="https://manpages.debian.org/cgi-bin/man.cgi?query=procmailrc">procmailrc(5)</a>에서
확인하고, FROM_DAEMON에 일치하지 않는 메일 주소를 사용해 다시 보내
보십시오.</p>


<h2>알려지지 않은 패키지에 버그 보고</h2>

<p>버그 추적 시스템이 관련 패키지의 메인테이너가 누구인지 알지
못한다면, <code>maintonly</code>를 사용하더라도 버그 보고서를
<code>debian-bugs-dist</code>에 전달합니다.</p>
<p><code>maintonly@bugs.debian.org</code> 또는
<var>nnn</var><code>-maintonly@bugs.debian.org</code>에 보낼 경우, 버그
보고서에 올바른 패키지를 지정하도록 해야 합니다. 최초 보고서를 보낼 때 맨 위에
올바른 <code>Package</code> 줄을 쓸 수 있고, 아니면 <A href="server-control">the
<code>control@bugs.debian.org</code> 서비스</A>에서 버그 할당을 바꿀 수도
있습니다.</p>


<h2><a name="findpkgver"><code>dpkg</code>를 사용해 버그 보고에 필요한
패키지와 버전 찾기</a></h2>

<p>명령으로 버그를 보고할 때 <code>reportbug</code>를 사용하는 경우, 예를 들어
<code>grep</code>이라고 하면, 다음과 같이 하면 자동으로 해당 패키지를 선택해
바로 보고서를 작성하도록 합니다: <code>reportbug --file $(which
grep)</code></p>

<p><code>dpkg --search</code> 명령을 사용해 어떤 패키지가 설치했는지 찾아볼
수도 있습니다. 어떤 버전의 패키지를 설치했는지 <code>dpkg --list</code>
또는 <code>dpkg --status</code> 명령으로 찾을 수 있습니다.
</p>

<p>예를 들어:</p>
<pre>
$ which apt-get
/usr/bin/apt-get
$ type apt-get
apt-get is /usr/bin/apt-get
$ dpkg --search /usr/bin/apt-get
apt: /usr/bin/apt-get
$ dpkg --list apt
Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Installed/Config-files/Unpacked/Failed-config/Half-installed
|/ Err?=(none)/Hold/Reinst-required/X=both-problems (Status,Err: uppercase=bad)
||/ Name           Version        Description
+++-==============-==============-============================================
ii  apt            0.3.19         Advanced front-end for dpkg
$ dpkg --status apt
Package: apt
Status: install ok installed
Priority: standard
Section: base
Installed-Size: 1391
Maintainer: APT Development Team &lt;deity@lists.debian.org&gt;
Version: 0.3.19
Replaces: deity, libapt-pkg-doc (&lt;&lt; 0.3.7), libapt-pkg-dev (&lt;&lt; 0.3.7)
Provides: libapt-pkg2.7
Depends: libapt-pkg2.7, libc6 (&gt;= 2.1.2), libstdc++2.10
Suggests: dpkg-dev
Conflicts: deity
Description: Advanced front-end for dpkg
 This is Debian's next generation front-end for the dpkg package manager.
 It provides the apt-get utility and APT dselect method that provides a
 simpler, safer way to install and upgrade packages.
 .
 APT features complete installation ordering, multiple source capability
 and several other unique features, see the Users Guide in
 /usr/doc/apt/guide.text.gz

</pre>

<a name="otherusefulcommands"></a>
<h2>다른 쓸모 있는 명령 및 패키지</h2>

<p>
<a href="https://packages.debian.org/stable/utils/reportbug">reportbug</a>와
같은 패키지에 들어 있는 <kbd>querybts</kbd> 도구는 버그 추적 시스템에 대한
편리한 텍스트 기반 인터페이스입니다.</p>

<p>이맥스 사용자는
<code><a href="https://packages.debian.org/stable/utils/debian-el">\
debian-el</a></code> 패키지의 debian-bug 명령을 사용할 수도 있습니다.
<kbd>M-x debian-bug</kbd>로 시작하면, <code>reportbug</code>와 비슷한
방법으로 필요한 정보를 모두 물어봅니다.</p>

<hr />

#use "otherpages.inc"

#use "$(ENGLISHDIR)/Bugs/footer.inc"

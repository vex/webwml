#use wml::debian::translation-check translation="645e78e5a0801f8f9bd112dbe4eb9b199dcbf331"
<define-tag pagetitle>Het Debian Project rouwt om het verlies van Jérémy Bobbio (Lunar)</define-tag>
<define-tag release_date>2024-11-19</define-tag>
#use wml::debian::news

<p>Met droefheid meldt het Debian Project het nieuws van het overlijden van
Jérémy Bobbio (Lunar) op vrijdag 8 november 2024.</p>

<p>Lunar genoot grote erkenning en respect in verschillende gebieden van de
Linuxgemeenschap en die van Free/Libre Open Source Software (FLOSS); hij was
evenzeer bekend als een fervent voorstander van individuele en collectieve
vrijheden.</p>

<p>Lunar was toegewijd aan en altijd aanwezig bij de formulering en de
bespreking van <a href="https://reproducible-builds.org/">Reproduceerbare
Builds</a>. Voor veel mensen kwam hun kennismaking met dit controlesysteem voor
software direct uit zijn mond tijdens talrijke conferenties, lezingen en op
mailinglijsten.</p>

<p>Lunar was ook een moedige voorstander van en een uitgesproken ambassadeur
voor bezorgdheden in verband met toezicht en censuur en de bescherming van
privacy, wat hem ertoe aanzette om binnen het
<a href="https://torproject.org/">Tor Project</a> te werken en er relaties en
software te ontwikkelen. Lunar was van cruciaal belang bij het helpen van het
netwerk om hulpmiddelen te ontwikkelen, relais op te zetten en juridische
problemen voor exploitanten en beheerders aan te pakken.</p>

<p>Het overlijden van Lunar laat een leegte achter in veel gemeenschappen. We
zullen doorgaan, we zullen allemaal de gebieden die voor hem belangrijk waren
blijven uitdiepen, en we zullen dat werk voortzetten in zijn nalatenschap.
Lunar, je zult gemist worden!</p>

<p>Overal ter wereld klonk eerbetoon en werden herinneringen opgehaald toen het
nieuws over Lunars overlijden bekend werd. Hieronder delen we enkele van die
gedachten:</p>

<blockquote>
<p>Zijn werk aan Reproduceerbare Builds, samen met enkele andere mensen op deze
lijst, betekende voor Debian een ommekeer en, durf ik te zeggen, zijn tijd ver
vooruit voor een sector die langzaam de enorme bedreiging ontdekt die aanvallen
op de toeleveringsketen vormen (meest recent met XZ).</p>
</blockquote>
<p>&mdash;&nbsp;Faidon Liambotis</p>

<blockquote>
<p>Ik heb altijd gelet op en aandacht besteed aan zijn oog voor detail bij zaken
die anderen als triviaal zouden hebben beschouwd. Door zijn inzicht en
uitleg werd duidelijk hoe cruciaal die kleine details waren voor een goed
begrip. Een echte mentor.</p>
</blockquote>
<p>&mdash;&nbsp;Donald Norwood</p>

<blockquote>
<p>Lunar was een ongelooflijk persoon, en ik mis hem diep, ondanks dat ik hem
maar af en toe zag. Creatief, bedachtzaam, slim en vriendelijk, maar niet
ondoordacht. Stekelig wanneer stekeligheid nodig was, maar ook bereid om de hand
te reiken of goedgunstig te zijn wanneer dat nodig was. Ik zal zijn scherpe
geest, doordringende observaties en zijn uitdagingen om een beter mens te worden
missen.</p>
</blockquote>

<blockquote>
<p>De mensen die werken aan vrije software en aanverwante projecten vormen een
betere gemeenschap doordat Lunar onder ons was. Ik blijf proberen te voldoen
aan de normen die Lunar heeft gesteld.</p>
</blockquote>
<p>&mdash;&nbsp;dkg</p>

<blockquote>
<p>Ik herinner me vooral een van de laatste gesprekken die ik een paar maanden
geleden met Lunar had, toen hij me vertelde hoe trots hij was, niet alleen omdat
hij Nos Oignons had gestart en bijgedragen had aan de start van Reproduceerbare
Builds, maar vooral over het feit dat beide initiatieven nu bloeiden zonder
afhankelijk van hem te zijn. Hij dacht waarschijnlijk aan een toekomstige wereld
zonder hem, maar besefte ook hoe invloedrijk zijn activisme was geweest op de
wereld van vroeger en nu.</p>
</blockquote>
<p>&mdash;&nbsp;Stefano Zacchiroli</p>

<blockquote>
<p>Ik kan me best wat momenten herinneren, maar grappig genoeg denk ik het
vaakst aan de ontmoeting met hem op de campus in Portland tijdens DebConf14,
toen hij net terugkwam van het kopen van een grote doos vegan donuts om deze uit
te delen. Hij was daar zo blij om! Meer ernstig denk ik dat zijn impact op Tor,
Debian en Reproduceerbare Builds nauwelijks kan worden overschat en dan is er
nog een breed gebied van "offline zaken" waar hij ook veel om gaf, en toch
bleef hij altijd bescheiden en behulpzaam. Ik ben ongelooflijk dankbaar voor al
zijn werk en voor het feit dat we wat hebben kunnen samenwerken en samen goede
momenten hebben kunnen beleven.</p>
</blockquote>
<p>&mdash;&nbsp;Holger Levsen</p>

<h2>Over Debian</h2>
<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>
<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a> of stuur een e-mail naar
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>

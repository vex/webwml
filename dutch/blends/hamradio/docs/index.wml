#use wml::debian::blend title="Documentatie"
#use "../navbar.inc"
#use wml::debian::translation-check translation="27a7be18daa00f21db14cd22b5eeb82ca0902330"

<p>
Er zijn veel documentatiebronnen beschikbaar voor het gebruik van amateurradiosoftware in Debian. Alle softwarepakketten hebben man-pagina's en veel pakketten bevatten ook documentatie in HTML- of PDF-formaat.
</p>

<p>
<em>Een uitgebreide handleiding voor het gebruik van amateurradiosoftware in Debian is momenteel in ontwikkeling, maar is nog niet beschikbaar. Als u hieraan wilt bijdragen, ga dan naar <a href="https://wiki.debian.org/DebianHams/Handbook">de gerelateerde wiki-pagina</a>.</em>
</p>

<h2>man-pagina's</h2>

<p>
Volgens het beleid van Debian moet elke toepassing in Debian een man-pagina bevatten en deze kunnen zeer informatief zijn. Ze zijn doorgaans nuttiger voor toepassingen aan de commandoregel, maar kunnen ook nuttige informatie bieden voor toepassingen met een grafische gebruikersinterface. Om de man-pagina van een geïnstalleerde toepassing te bekijken, voert u het volgende commando uit:
</p>

<pre>$ man &lt;toepassing&gt;</pre>

<p>
Als u de man-pagina wilt bekijken van een toepassing die u momenteel niet heeft geïnstalleerd, kunt u zoeken op <a href="https://manpages.debian.org/">manpages.debian.org</a>.
</p>


#use wml::debian::template title="Debian 12 -- Erratas" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="7902f9806602753a3b1636df05e2f6d51fc77c40"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Known problems</toc-add-entry>
<toc-add-entry name="security">Problemas de seguridad</toc-add-entry>

<p>El equipo de seguridad de Debian publica actualizaciones de paquetes de la versión «estable»
en los cuales ha identificado problemas relacionados con la seguridad. Consulte las
<a href="$(HOME)/security/">páginas de seguridad</a> para información sobre
cualquier problema de seguridad identificado en <q>bookworm</q>.</p>

<p>Si usa APT, agregue la siguiente línea en <tt>/etc/apt/sources.list</tt>
para tener acceso a las últimas actualizaciones de seguridad:</p>

<pre>
  deb http://security.debian.org/ bookworm-security main contrib non-free non-free-firmware
</pre>

<p>Después, ejecute <kbd>apt update</kbd> seguido de
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Versiones</toc-add-entry>

<p>A veces, en el caso de problemas críticos o actualizaciones de seguridad, la
distribución publicada se actualiza. Generalmente, esto se indica con un nuevo número de versión («point
release» en inglés).</p>

<ul>
  <li>La primera versión, 12.1, se publicó el
      <a href="$(HOME)/News/2023/20230722">22 de julio de 2023</a>.</li>
  <li>La segunda versión, 12.2, se publicó el
      <a href="$(HOME)/News/2023/20231007">7 de octibre de 2023</a>.</li>
  <li>La tercera versión, 12.3, fue pospuesta el
      <a href="$(HOME)/News/2023/2023120902">9 de diciembre de 2023</a>.</li>
  <li>La cuarta versión, 12.4, se publicó el
     <a href="$(HOME)/News/2023/20231210">10 de diciembre de 2023</a>.</li>
  <li>La quitna versión, 12.5, se publicó el
     <a href="$(HOME)/News/2024/20240210">10 de febrero de 2024</a>.</li>
  <li>La sexta versión, 12.6, se publicó el
     <a href="$(HOME)/News/2024/20240629">29 de junio de 2024</a>.</li>
  <li>La séptima versión, 12.7, se publicó el
     <a href="$(HOME)/News/2024/20240831">31 de agosto de 2024</a>.</li>

</ul>


<ifeq <current_release_bookworm> 12.0 "

<p>Aún no hay versiones posteriores de Debian 12.</p>" "

<p>Vea el <a
href="http://deb.debian.org/debian/dists/bookworm/ChangeLog">\
registro de cambios («ChangeLog»)</a>
para detalles sobre los cambios entre la versión 12 y la <current_release_bookworm/>.</p>"/>


<p>Las correcciones para la distribución «estable» publicada pasan un
período de pruebas extendido antes de ser aceptadas en el archivo.
Sin embargo, estas correcciones están disponibles en el directorio
<a href="http://ftp.debian.org/debian/dists/bookworm-proposed-updates/">\
dists/bookworm-proposed-updates</a> de cualquier réplica del archivo de
Debian.</p>

<p>Si usa APT para actualizar los paquetes, puede instalar
las actualizaciones propuestas agregando la siguiente línea en
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# adiciones propuestas para una versión de Debian 12
  deb https://deb.debian.org/debian bookworm-proposed-updates main contrib non-free-firmware non-free
</pre>

<p>Después, ejecute <kbd>apt update</kbd> seguido de
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema de instalación</toc-add-entry>

<p>
Para información sobre erratas y actualizaciones del sistema de instalación, vea
la página de <a href="debian-installer/">información de instalación</a>.
</p>

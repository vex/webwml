#use wml::debian::template title="Installering af Debian via internettet" BARETITLE=true
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Denne metode til at installere Debian på, kræver en fungerende 
internetforbindelse <em>under</em> installeringen.  Sammenlignet med andre 
metoder, ender man med at hente færre data eftersom processen er skræddersyet 
til ens behov.  Ethernet- og trådløse forbindelser er understøttet.  Interne
ISDN-kort er desværre <em>ikke</em> understøttet.</p>

<p>Der er tre muligheder for installeringer over netværket:</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">Små cd'er eller USB-pinde</toc-add-entry>

<p>De følgende er aftryksfiler.  Vælg din processorarkitektur herunder.</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>For yderligere oplysninger, se: <a href="../CD/netinst/">Netværksinstallering
fra en minimal cd</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">Meget små cd'er, fleksible USB-pinde, osv.</toc-add-entry>

<p>Du kan hente et par små aftryksfiler, beregnet til USB-pinde og lignende 
enheder.  Skrive dem til mediet, og dernæst iværksætte installeringen ved at 
starte fra mediet.</p>

<p>De er lidt variation i understøttelse af installering fra forskellige meget
små aftryk hvad angår de forskellige arkitekturer.
</p>

<p>For uddybende oplysninger, se <a href="$(HOME)/releases/stable/installmanual">\
installeringshåndbogen til din arkitektur</a>, særligt afsnittet <q>Obtaining 
System Installation Media</q>.</p>

<p>
Her er der links til de
tilgængelige aftryksfiler (se filen MANIFEST for information):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">Start over netværket</toc-add-entry>

<p>Du kan opsætte en TFTP- og en DHCP-server (eller BOOTP, eller RARP), der 
fungerer som installeringsmedium til maskinerne på dit lokale netværk.  Hvis
din klientmaskines BIOS understøtter det, kan du starte Debians 
insterllingssystem fra netværket (vha. PXE og TFTP), og fortsætte med at
installere resten af Debian fra netværket.</p>

<p>Ikke alle maskiner understøtter start over netværket.  På grund af det ekstra
arbejde, det kræver, er denne metode til at installere Debian på, ikke beregnet
til uerfarne brugere.</p>

<p>For uddybende oplysninger, se <a href="$(HOME)/releases/stable/installmanual">\
installeringshåndbogen til din arkitektur</a>, særligt afsnittet <q>Preparing 
Files for TFTP Net Booting</q>.</p>

<p>Her finder du links til aftryksfilerne (se filen MANIFEST for 
information):</p>

<stable-netboot-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">

<p>Hvis noget af hardwaren i dit system <strong>kræver at ikke-fri firmware skal 
indlæses</strong> sammen med enhedsdriveren, kan du anvende en 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tarball med almindelige firmwarepakker</a> eller hente et 
<strong>uofficielt</strong> aftryk, som indehodler denne 
<strong>ikke-frie</strong> firmware.  Vejledning i hvordan en tarball anvendes 
og generelle oplysninger om indlæsning af firmware under en installering, finder 
man i <a href="../../releases/stable/amd64/ch06s04">Installation Guide</a>.</p>

<p><a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">\
uofficielle installeringsaftryk til udgaven <q>stable</q> med medfølgende firmware</a></p>

</div>

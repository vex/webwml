#use wml::debian::template title="LTS Security Information" GEN_TIME="yes"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<aside style="background-color:transparent;">
  <img src="../../Pics/Debian-LTS-2.png" width="200">
</aside>

<toc-display/>

<toc-add-entry name="keeping-secure">Keeping your Debian LTS system secure</toc-add-entry>

<p>The package <a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a>
keeps the computer current with the latest security (and other) updates automatically.
The <a href="https://wiki.debian.org/UnattendedUpgrades">wiki</a> has
detailed information about how to set up the package.</p>

<p>For more information about security issues in Debian, please refer to
the <a href="../../security">Debian Security Information</a>.</p>

<toc-add-entry name="DLAS">Recent Advisories</toc-add-entry>

<p>These are the recent Debian LTS Advisory (DLA) posted to
the <a href="https://lists.debian.org/debian-lts-announce/">debian-lts-announce</a> list.
<a class="rss_logo" style="float: none;" href="dla">RSS</a>
</p>

<p>
#include "$(ENGLISHDIR)/lts/security/dla.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}


<toc-add-entry name="infos">Sources of Security Information</toc-add-entry>
#include "../../security/security-sources.inc"

<ul>
<li> <a href="https://lts-team.pages.debian.net/wiki/FAQ">Debian LTS FAQ</a>. Your question may well be answered there already!
<li>
<a href="https://lts-team.pages.debian.net/wiki/Contact">Debian LTS team contact information</a>
</ul>

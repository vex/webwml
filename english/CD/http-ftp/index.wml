#use wml::debian::cdimage title="Downloading Debian USB/CD/DVD images via HTTP/FTP" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<p>The following Debian images are available for
download:</p>

<ul>

  <li><a href="#stable">Official USB/CD/DVD images of the <q>stable</q> release</a></li>

  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Official
  USB/CD/DVD images of the <q>testing</q> distribution (<em>regenerated
  weekly</em>)</a></li>

</ul>

<p>See also:</p>
<ul>

  <li>A complete <a href="#mirrors">list of <tt>debian-cd/</tt> mirrors</a></li>

  <li>For <q>network install</q> images,
  see the <a href="../netinst/">network install</a> page.</li>


  <li>For images of the <q>testing</q> release, see the <a
  href="$(DEVEL)/debian-installer/">Debian-Installer page</a>.</li>

</ul>

<hr />

<h2><a name="stable">Official USB/CD/DVD images of the <q>stable</q> release</a></h2>

<p>To install Debian on a machine without an Internet connection,
it's possible to use CD/USB images (700&nbsp;MB each) or DVD/USB images (4.7&nbsp;GB each).
Download the first CD/USB or DVD/USB image file, write it using a USB/CD/DVD
recorder, and then reboot from that.</p>

<p>The <strong>first</strong> USB/CD/DVD disk contains all the files necessary
to install a standard Debian system.<br />
</p>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>

<p>The following links point to image files which are up to 700&nbsp;MB
in size, making them suitable for writing to normal CD-R(W) media:</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>

<p>The following links point to image files which are up to 4.7&nbsp;GB
in size, making them suitable for writing to normal DVD-R/DVD+R and
similar media:</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Be sure to have a look at the documentation before you install.
<strong>If you read only one document</strong> before installing, read our
<a href="$(HOME)/releases/stable/amd64/apa">Installation Howto</a>, a quick
walkthrough of the installation process. Other useful documentation includes:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installation Guide</a>,
    the detailed installation instructions</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer
    Documentation</a>, including the FAQ with common questions and answers</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    Errata</a>, the list of known problems in the installer</li>
</ul>

<hr />

<h2><a name="mirrors">Registered mirrors of the <q>debian-cd</q> archive</a></h2>

<p>Note that <strong>some mirrors may not be up to date</strong> &mdash;
the current release of the "stable" USB/CD/DVD images is <strong><current-cd-release></strong>.

<p><strong>If in doubt, use the <a href="https://cdimage.debian.org/debian-cd/">primary
CD image server</a> in Sweden,</strong></p>

<p>Are you interested in offering the Debian CD images on your
mirror? If yes, see the <a href="../mirroring/">instructions on
how to set up a CD image mirror</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"

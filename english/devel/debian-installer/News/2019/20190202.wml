<define-tag pagetitle>Debian Installer Buster Alpha 5 release</define-tag>
<define-tag release_date>2019-02-02</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the fifth alpha release of the installer for Debian 10
<q>Buster</q>.
</p>


<h2>Important change in this release</h2>

<p>The Debian Installer team is happy to report that the Buster Alpha
  5 release of the installer includes some initial support for UEFI
  Secure Boot (SB) in Debian's installation media.</p>

<p>This support is not yet complete, and we would like to request some
  help! A detailed article with more background about Secure Boot and
  with a pointer to instructions on how to test and report
  findings <a href="https://bits.debian.org/2019/02/testing-initial-secure-boot-support.html">has
  been published separately.</a></p>


<h2>Improvements in this release</h2>

<ul>
  <li>btrfs-progs:
    <ul>
      <li>Add support for libzstd.</li>
    </ul>
  </li>
  <li>cdebconf:
    <ul>
      <li>Notify gtk-font-set of the zoom factor.</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Bump Linux kernel ABI from 4.18.0-3 to 4.19.0-1.</li>
    </ul>
  </li>
  <li>debootstrap:
    <ul>
      <li>Add runtime optimizations (<a href="https://bugs.debian.org/871835">#871835</a>).</li>
    </ul>
  </li>
  <li>fonts-noto:
    <ul>
      <li>Fix Gujarati support by including Noto Serif Gujarati in
        main udeb package (<a href="https://bugs.debian.org/911705">#911705</a>, <a href="https://bugs.debian.org/915825">#915825</a>).</li>
    </ul>
  </li>
  <li>grub-installer:
    <ul>
      <li>Add support for shim-signed and grub-efi-amd64-signed,
        default to installing signed packages on amd64.</li>
    </ul>
  </li>
  <li>grub2:
    <ul>
      <li>Add luks modules to signed UEFI images (<a href="https://bugs.debian.org/908162">#908162</a>).</li>
      <li>Backport Xen PVH guest support from upstream (<a href="https://bugs.debian.org/776450">#776450</a>).</li>
      <li>Improve arm/arm64 initrd handling (<a href="https://bugs.debian.org/907596">#907596</a>, <a href="https://bugs.debian.org/909420">#909420</a>,
        <a href="https://bugs.debian.org/915091">#915091</a>).</li>
      <li>Don't enforce Shim signature validation if Secure Boot is
        disabled.</li>
    </ul>
  </li>
  <li>localechooser:
    <ul>
      <li>Add missing English string "Choose language" to some
        translations: Bosnian, Hungarian, Tajik.</li>
    </ul>
  </li>
  <li>partconf:
    <ul>
      <li>Remove reiserfs support (<a href="https://bugs.debian.org/717534">#717534</a>).</li>
    </ul>
  </li>
  <li>partman-lvm:
    <ul>
      <li>Fix invalid characters in volume group names (<a href="https://bugs.debian.org/911036">#911036</a>).</li>
    </ul>
  </li>
  <li>partman-md:
    <ul>
      <li>When creating a new RAID1/5/6/10 array, start syncing at the
        minimum speed allowed by the system, instead of letting it run
        at full speed, slowing down package installation
        (<a href="https://bugs.debian.org/838503">#838503</a>).</li>
    </ul>
  </li>
  <li>rootskel-gtk:
    <ul>
      <li>Use Noto Serif Gujarati font for Gujarati (<a href="https://bugs.debian.org/911705">#911705</a>,
        <a href="https://bugs.debian.org/915825">#915825</a>).</li>
      <li>Apply font zoom set by cdebconf shortcut.</li>
    </ul>
  </li>
  <li>win32-loader:
    <ul>
      <li>Embed debian-archive-removed-keys keyring to ensure
        signature verification is performed successfully.</li>
    </ul>
  </li>
  <li>wpa:
    <ul>
      <li>Re-enable TLSv1.0 and security level 1 for wpasupplicant
        (<a href="https://bugs.debian.org/907518">#907518</a>, <a href="https://bugs.debian.org/911297">#911297</a>).</li>
    </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>debian-installer:
    <ul>
      <li>[arm64] Use arm-trusted-firmware instead of atf-allwinner to
        build pine64_plus and pinebook images.</li>
      <li>[armhf] Re-enable Firefly-RK3288 image.</li>
      <li>[armhf] Update Build-Depends for u-boot-rockchip.</li>
    </ul>
  </li>
  <li>flash-kernel:
    <ul>
      <li>Add machine db entry for Odroid HC1 (<a href="https://bugs.debian.org/916980">#916980</a>).</li>
      <li>Add machine db entry for Seagate Blackarmor NAS220
        (<a href="https://bugs.debian.org/918193">#918193</a>).</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>udeb: Define mtd-core-modules package to contain MTD core if
        not built-in.</li>
      <li>udeb: Move MTD core from nic-modules to
        mtd-core-modules.</li>
    </ul>
  </li>
  <li>partman-partitioning:
    <ul>
      <li>Set the default disklabel type for RISC-V-based systems to
        gpt.</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>76 languages are supported in this release.</li>
  <li>Full translation for 31 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>

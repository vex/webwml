#use wml::debian::cdimage title="Immagini «live» installabili"
#use wml::debian::release_info
#use wml::debian::installer
#use wml::debian::translation-check translation="7791836a42fa0b102a9f31cfc78e2c4345504388" maintainer="Giuseppe Sacco"

#include "$(ENGLISHDIR)/releases/images.data"

<p>Un'immagine <q>installabile «live»</q> contiene un sistema Debian in grado di
avviarsi senza modificare alcun file sul disco fisso, e permette anche
l'installazione di Debian a partire dal contenuto dell'immagine stessa.</p>

<p><a name="choose_live"><strong>È conveniente usare un'immagine «live»?</strong></a> Ecco alcune
considerazioni che aiuteranno nella decisione.
<ul>
<li><b>Versioni:</b> le immagini «live» sono disponibili in varie <q>versioni</q>
a seconda dell'ambiente desktop fornito (GNOME, KDE, LXDE, Xfce, Cinnamon e
MATE).
<li><b>Architettura:</b> al momento sono fornite soltanto le immagini per PC a 64-bit (amd64).</li>
<li><b>Installatore:</b> le immagini «live» mettono a disposizione
l'<a href="https://calamares.io">installatore Calamares</a> intuitivo, un sistema indipendente
dalla distribuzione.</li>
<li><b>Lingue:</b> le immagini non contengono i pacchetti per la totalit&agrave;
delle lingue. Se dovessero essere necessari metodi di input, set di caratteri e
pacchetti supplementari per la localizzazione, bisognerà installarli in
seguito.</li>
</ul>

<div class="line">
<div class="item col50">
<h2 id="live-install-stable">Immagini «live» ufficiali per il rilascio <q>stabile</q></h2>

<p>Queste immagini sono ideali per provare un sistema Debian e poi installarlo
dallo stesso supporto. Possono essere scritte su chiavi USB e su DVD-R(W)</p>
    <ul class="quicklist downlist">
      <li><a title="Scarica ISO live di Gnome per PC Intel o AMD a 64 bit"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Scarica ISO live di Xfce per PC Intel o AMD a 64 bit"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="Scarica ISO live di KDE per PC Intel o AMD a 64 bit"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Scarica ISO live per PC Intel o AMD"
            href="<live-images-url/>/amd64/iso-hybrid/">Altre ISO live</a></li>
      <li><a title="Scarica torrent per PC Intel o AMD a 64 bit"
          href="<live-images-url/>/amd64/bt-hybrid/">torrent live</a></li>
    </ul>
</div>

<div class="item col50 lsatcol">
<h2 id="live-install-testing">Immagini d'installazione ufficiali «live» per il rilascio <q>testing</q></h2>

<p>Queste immagini sono ideali per provare un sistema Debian e poi installarlo
dallo stesso supporto. Possono essere scritte su chiavi USB e su DVD-R(W)</p>
    <ul class="quicklist downlist">
      <li>Immagini TESTING:</li>
      <li><a title="Scarica ISO live di Gnome per PC Intel o AMD a 64 bit"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Scarica ISO live di Xfce per PC Intel o AMD a 64 bit"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="Scarica ISO live di KDE per PC Intel o AMD a 64 bit"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Scarica ISO live per PC Intel o AMD a 64 bit"
            href="<live-images-testing-url/>/amd64/iso-hybrid/">Altre ISO live</a></li>
    </ul>
</div>

</div>

<p>Per informazioni su cosa siano questi file e su come usarli, visitare
le <a href="../faq/">FAQ</a>.</p>

<p>Visitare la <a href="$(HOME)/devel/debian-live">pagina del progetto Debian
Live</a> per ulteriori informazioni sui sistemi Live Debian forniti da
queste immagini.</p>


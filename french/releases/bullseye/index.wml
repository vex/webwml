#use wml::debian::template title="Informations sur la version «&nbsp;Bullseye&nbsp;» de Debian"
#use wml::debian::translation-check translation="edbe7403c8846f9f66e83e00e7152cfbf0bacace" maintainer="Jean-Pierre Giraud"
#include "$(ENGLISHDIR)/releases/info"

<p>
La version&nbsp;<current_release_bullseye> de Debian (connue sous le nom
de <em>Bullseye</em>) a été publiée le
<a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
  "La version 11.0 a été initialement publiée le <:=spokendate('2021-08-14'):>."
/>
Cette version comprend de nombreuses modifications décrites dans notre
<a href="$(HOME)/News/2021/20210814">communiqué de presse</a> et les <a
href="releasenotes">notes de publication</a>.
</p>

<p><strong>Debian 11 a été remplacée par
<a href="../bookworm/">Debian 12 (<q>Bookworm</q>)</a>.
</strong></p>

<p>
Le cycle de vie de Debian 11 couvre cinq ans : une prise en charge complète
par Debian pendant les trois premières années, jusqu'au
<:=spokendate('2024-08-14'):>, puis deux ans de prise en charge à long terme
(Long Term Support – LTS) jusqu'au <:=spokendate('2026-08-31'):>. Le nombre
d'architectures prises en charge est limité à i386, amd64, armhf et arm64
pendant la durée de prise en charge à long terme de Bullseye. Pour de plus
amples informations, veuillez consulter la page du site web consacrée aux
<a href="$(HOME)/security/">informations de sécurité</a> et la
<a href="https://wiki.debian.org/LTS">section LTS du Wiki de Debian</a>.
</p>

<p>
Pour obtenir et installer Debian, veuillez vous reporter à la page des
informations d'installation et au guide d'installation. Pour mettre à
niveau à partir d'une ancienne version de Debian, veuillez vous reporter aux
instructions des <a href="releasenotes">notes de publication</a>.
</p>

<p>
Les architectures suivantes étaient gérées par cette version au moment de
sa publication&nbsp;:
</p>

<ul>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/arm64/">ARM64 bits (AArch64)</a>
<li><a href="../../ports/armel/">ARM EABI (armel)</a>
<li><a href="../../ports/armhf/">ARM avec unité de calcul flottant (armhf)</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/mips/">MIPS (grand boutiste)</a>
<li><a href="../../ports/mipsel/">MIPS (petit boutiste)</a>
<li><a href="../../ports/mips64el/">MIPS 64 bits (petit boutiste)</a>
<li><a href="../../ports/ppc64el/">Processeurs POWER</a>
<li><a href="../../ports/s390x/">IBM System z</a>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous signaler d'autres problèmes</a>.
</p>

# From: r.meier@siemens.com
# Webpage URL is usually redirected, this is normal, Roger Meier 2005-09-20
#use wml::debian::translation-check translation="7dddaeaa5c20d8b39e4a465987ff2f4526a8451" maintainer="Jean-Pierre Giraud"

<define-tag pagetitle>Siemens</define-tag>
<define-tag webpage>https://www.siemens.com/</define-tag>

#use wml::debian::users
#use wml::debian::translation-check translation="91ec6965d63bf43e927c406170b09824c54b1e0c" maintainer="Jean-Pierre Giraud"

<p>
  Siemens utilise Debian dans différents domaines dont la recherche et le
  développement, les infrastructures, les produits et ses solutions.
</p>
<p>    
  Debian est utilisée comme base de nombre de nos produits et services en
  automation et numérisation de processus et d'industries manufacturières, de
  nos solutions de mobilité intelligente pour la route et le rail, de nos
  services de santé numériques et de technologie médicale aussi bien que pour
  nos systèmes d'énergie décentralisés et nos infrastructures intelligentes
  pour les bâtiments.
</p>
<p>
  Par exemple, Debian est embarquée et fait partie des 
  <a href="https://www.siemens-healthineers.com/magnetic-resonance-imaging">scanners IRM de Siemens</a>, 
  des systèmes modernes embarqués de trains et métros de Siemens, elle est dans
  l'environnement des
  <a href="https://press.siemens.com/global/en/feature/siemens-digitalize-norwegian-railway-network">lignes et des gares ferroviaires </a>,
  et dans beaucoup d'autres de nos appareils. Siemens est en outre un membre
  fondateur de la 
  <a href="https://www.cip-project.org/">Plateforme d'infrastructures civiles</a>,
  utilisant Debian LTS comme couche de base.
</p>
<p>
  Il va sans dire que Debian est aussi un élément essentiel de notre
  infrastructure de serveurs. Nos stations de travail et nos machines de bureau
  fonctionnent souvent avec Debian chaque fois que Linux est requis. Par
  exemple, la plupart des produits basés sur Linux dans le domaine de la
  technologie du bâtiment sont développés sur Debian. Siemens offre aussi des
  distributions embarquées basées sur Debian telles que
  <a href="https://siemens.com/embedded">Sokol™ Omni OS</a>.  
</p>
<p>
  Pourquoi nous sommes attachés à Debian : la haute qualité et la stabilité que
  Debian nous permet d'atteindre est simplement fantastique, et la
  disponibilité des mises à jour des logiciels et de leurs correctifs de
  sécurité est excellente et la quantité de paquets logiciels disponibles est
  extraordinaire.
</p>

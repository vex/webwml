#use wml::debian::translation-check translation="f93b89c74962ae015feaa58f013cef8f7ec08dd9" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.11</define-tag>
<define-tag release_date>2024-08-31</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la onzième et dernière mise à jour de
sa distribution oldstable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Secure Boot et les autres systèmes d'exploitation</h2>

<p>
Les utilisateurs qui démarrent d'autres systèmes d'exploitation sur la
même machine et où le démarrage sécurisé (Secure Boot) est activé, doivent
savoir que shim 15.8 (inclus dans Debian <revision>) révoque les signatures
de toutes les versions antérieures de shim dans le microprogramme UEFI.
Cela peut empêcher de démarrer tous les autres systèmes d'exploitation
utilisant des versions de shim antérieures à 15.8.
</p>

<p>
Les utilisateurs affectés peuvent désactiver temporairement le démarrage
sécurisé avant de mettre à jour les autres systèmes d’exploitation.
</p>

<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version oldstable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction amd64-microcode "Nouvelle version amont ; corrections de sécurité [CVE-2023-31315] ; corrections du microprogramme SEV [CVE-2023-20584 CVE-2023-31356]">
<correction ansible "Nouvelle version amont stable ; correction d'un problème d'injection de modèle [CVE-2021-3583], d'un problème de divulgation d'informations [CVE-2021-3620], d'un problème d'écrasement de fichier [CVE-2023-5115], correction d'un problème d'injection de modèle [CVE-2023-5764], de problèmes de divulgation d'informations [CVE-2024-0690 CVE-2022-3697] ; documentation d'une solution de contournement d'une divulgation de clé ec2 privée [CVE-2023-4237]">
<correction apache2 "Nouvelle version amont stable ; correction d'un problème de divulgation de contenu [CVE-2024-40725]">
<correction base-files "Mise à jour pour cette version">
<correction bind9 "Configuration autorisée des limites introduites pour corriger le CVE-2024-1737">
<correction calibre "Correction d'un problème de script intersite [CVE-2024-7008], d'un problème d'injection de code SQL [CVE-2024-7009]">
<correction choose-mirror "Mise à jour de la liste des miroirs disponibles">
<correction cjson "Ajout de vérification de NULL à cJSON_SetValuestring et à cJSON_InsertItemInArray [CVE-2023-50472 CVE-2023-50471 CVE-2024-31755]">
<correction cups "Correction de problèmes de gestion de socket de domaine [CVE-2024-35235] ; correction d'une régression quand uniquement des sockets de domaine sont utilisés">
<correction curl "Correction d'un problème de lecture hors limite lors de l'analyse de date d'ASN.1 [CVE-2024-7264]">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 5.10.0-32 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction dropbear "Correction du comportement <q>noremotetcp</q> des paquets keepalive en combinaison avec la restriction <q>no-port-forwarding</q> d'authorized_keys(5)">
<correction fusiondirectory "Rétro-portage de la compatibilité avec la version de php-cas traitant le CVE 2022-39369 ; correction d'un problème de gestion de session incorrecte [CVE-2022-36179] ; correction d'un problème de script intersite [CVE-2022-36180]">
<correction gettext.js "Correction d'un problème de contrefaçon de requête côté serveur [CVE-2024-43370]">
<correction glewlwyd "Correction d'un dépassement de tampon pendant l'assertion de signature webauthn [CVE-2022-27240] ; traversée de répertoire évitée dans static_compressed_inmemory_website_callback.c [CVE-2022-29967] ; copie de bootstrap, jquery et de fork-awesome à la place d'un lien ; dépassement de tampon lors de la validation de signature FIDO2 [CVE-2023-49208]">
<correction glibc "Correction d'un problème de performance de ffsll() en fonction de l’alignement du code ; amélioration des performances pour memcpy() sur arm64 ; correction de la régression de concernant l'année 2038 dans nscd à la suite du CVE-2024-33601 et du CVE-2024-33602">
<correction graphviz "Correction de la mise à l'échelle cassée">
<correction gtk+2.0 "Recherche évitée de modules dans le répertoire de travail en cours [CVE-2024-6655]">
<correction gtk+3.0 "Recherche évitée de modules dans le répertoire de travail en cours [CVE-2024-6655]">
<correction healpix-java "Correction d'un échec de construction">
<correction imagemagick "Correction de problèmes de division par zéro [CVE-2021-20312 CVE-2021-20313] ; correction d'un correctif incomplet pour le CVE-2023-34151">
<correction indent "Rétablissement de la macro ROUND_UP et ajustement de la taille initiale du tampon pour corriger des problèmes de gestion de mémoire ; correction d'une lecture en dehors du tampon dans search_brace()/lexi() ; correction d'écrasement de tampon de tas dans search_brace() [CVE-2023-40305] ; correction d'une lecture avant le début du tampon de tas dans set_buf_break() [CVE-2024-0911]">
<correction intel-microcode "Nouvelle version amont ; corrections de sécurité [CVE-2023-42667 CVE-2023-49141 CVE-2024-24853 CVE-2024-24980 CVE-2024-25939]">
<correction libvirt "Correction d'un problème de confinement de sVirt [CVE-2021-3631], d'une utilisation de mémoire après libération [CVE-2021-3975], de problèmes de déni de service [CVE-2021-3667 CVE-2021-4147 CVE-2022-0897 CVE-2024-1441 CVE-2024-2494 CVE-2024-2496]">
<correction midge "Exclusion de examples/covers/* pour des raisons de conformité à la DFSG ; ajout des cibles de construction build-arch/build-indep ; utilisation du format de paquet source quilt (3.0)">
<correction mlpost "Correction d'un échec de construction avec les versions récentes d'ImageMagick">
<correction net-tools "Suppression de la dépendance de construction à libdnet-dev">
<correction nfs-utils "Passage de tous les drapeaux d'export valables à nfsd">
<correction ntfs-3g "Correction d'une utilisation de mémoire après libération dans <q>ntfs_uppercase_mbs</q> [CVE-2023-52890]">
<correction nvidia-graphics-drivers-tesla-418 "Correction de l'utilisation de symboles uniquement GPL provoquant des échecs de construction">
<correction nvidia-graphics-drivers-tesla-450 "Nouvelle version amont stable">
<correction nvidia-graphics-drivers-tesla-460 "Nouvelle version amont stable">
<correction ocsinventory-server "Rétro-portage de la compatibilité avec la version de php-cas traitant le CVE 2022-39369">
<correction onionshare "Rétrogradation de la dépendance d'obfs4proxy à Recommends, pour permettre le retrait d'obfs4proxy">
<correction php-cas "Correction d'un problème d'exploitation du service de découverte de nom d'hôte [CVE-2022-39369]">
<correction poe.app "Cellules de commentaire éditables ; correction du dessin quand une <q>NSActionCell</q> dans les préférences applique un changement d'état">
<correction putty "Correction d'une génération de nombre arbitraire ECDSA faible permettant la récupération de la clé secrète [CVE-2024-31497]">
<correction riemann-c-client "Charge utile malformée évitée dans les opérations envoi/réception de GnuTLS">
<correction runc "Correction de l'URL de l'archive de busybox ; dépassement de tampon évité lors de l'écriture de messages netlink [CVE-2021-43784] ; correction des tests sur les noyaux récents ; accès en écriture empêchée à la hiérarchie du cgroup appartenant à l'utilisateur <q>/sys/fs/cgroup/user.slice/...</q> [CVE-2023-25809] ; correction d'une régression du contrôle d'accès [CVE-2023-27561 CVE-2023-28642]">
<correction rustc-web "Nouvelle version amont stable pour prendre en charge la construction des nouvelles versions de Chromium et de Firefox ESR">
<correction shim "Nouvelle version amont">
<correction shim-helpers-amd64-signed "Reconstruction avec shim 15.8.1">
<correction shim-helpers-arm64-signed "Reconstruction avec shim 15.8.1">
<correction shim-helpers-i386-signed "Reconstruction avec shim 15.8.1">
<correction shim-signed "Nouvelle version amont stable">
<correction symfony "Correction du chargement automatique de HttpClient">
<correction trinity "Correction d'un échec de construction en supprimant la prise en charge de DECNET">
<correction usb.ids "Mise à jour de la liste des données incluses">
<correction xmedcon "Correction d'un dépassement de tas [CVE-2024-29421]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2024 5718 org-mode>
<dsa 2024 5719 emacs>
<dsa 2024 5721 ffmpeg>
<dsa 2024 5722 libvpx>
<dsa 2024 5723 plasma-workspace>
<dsa 2024 5725 znc>
<dsa 2024 5726 krb5>
<dsa 2024 5727 firefox-esr>
<dsa 2024 5728 exim4>
<dsa 2024 5729 apache2>
<dsa 2024 5730 linux-signed-amd64>
<dsa 2024 5730 linux-signed-arm64>
<dsa 2024 5730 linux-signed-i386>
<dsa 2024 5730 linux>
<dsa 2024 5734 bind9>
<dsa 2024 5736 openjdk-11>
<dsa 2024 5737 libreoffice>
<dsa 2024 5738 openjdk-17>
<dsa 2024 5739 wpa>
<dsa 2024 5740 firefox-esr>
<dsa 2024 5742 odoo>
<dsa 2024 5743 roundcube>
<dsa 2024 5746 postgresql-13>
<dsa 2024 5747 linux-signed-amd64>
<dsa 2024 5747 linux-signed-arm64>
<dsa 2024 5747 linux-signed-i386>
<dsa 2024 5747 linux>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction bcachefs-tools "Bogué ; obsolète">
<correction dnprogs "Bogué ; obsolète">
<correction iotjs "Pas entretenu, problèmes de sécurité">
<correction obfs4proxy "Problèmes de sécurité">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de oldstable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution oldstable :</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Mises à jour proposées à la distribution oldstable :</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication,
<i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>



<define-tag pagetitle>Versão Alfa 1 do instalador do Debian Trixie</define-tag>
<define-tag release_date>2024-12-31</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="7f67ce7f56845c84ba736aeea2464ff14d3b0649" maintainer="Daniel Martineschen"

<p>
A <a
href="https://wiki.debian.org/DebianInstaller/Team">equipe</a> do instalador
do Debian tem o prazer de anunciar a primeira versão alfa do instalador
para o Debian 13 <q>Trixie</q>.
</p>


<h2>Prefácio</h2>

Cyril Brulebois gostaria de dar agradecimentos especiais a Holger Wansing,
que realizou um maravilhoso trabalho percorrendo diversas mudanças propostas
em vários componentes do instalador, além de coordenar esforços de tradução.


<h2>Mudanças importantes nesta versão</h2>

<p>
Muitas mudanças já aconteceram durante o ciclo deste versão, e este anúncio
não pretende ser definitivo. Em vez disso, vamos nos ater a uma visão de 
alto nível das mudanças mais importantes.
</p>

<p>
Há grandes atualizações no lado do suporte a hardware:
</p>

<ul>
  <li>Não vamos mais construir um instalador para as arquiteturas armel e
      i386, mesmo elas permanecendo no repositório neste momento. </li>
  <li>A arquitetura mipsel foi removida do repositório ano passado.</li>
  <li>A arquitetura riscv64 é nova em folha!</li>
</ul>

<p>
Mesmo que as telas de boot ainda precisem ser atualizadas, o tema Ceratopsian
feito por Elise Couper está estreando no instalador.
</p>

<p>
As telas de configuração do(a) usuário(a) (que lidam com a criação do(a)
usuário(a) root e do(a) primeiro(a) usuário(a)) receberam uma reformulação
tardia.
</p>

<p>
Muitas melhorias e correções de bugs chegaram até componentes
responsáveis pelo particionamento. Isso inclui diferentes heurísticas
para particionamento automático (p. ex. para computar o tamanho do
swap), algumas receitas dedicadas a discos pequenos etc.
</p>


<h2>Status da localização</h2>

<ul>
  <li>78 idiomas são suportados nesta versão.</li>
  <li>18 deles têm tradução completa.</li>
</ul>


<h2>Problemas conhecidos nesta versão</h2>

<p>
Veja a <a href="$(DEVEL)/debian-installer/errata">errata</a> para detalhes
e uma lista completa de problemas conhecidos.
</p>


<h2>Feedback para esta versão</h2>

<p>
Precisamos da sua ajuda para encontrar bugs e melhorar ainda mais o instalador,
então por favor teste-o. CDs de instalação, outras mídias e tudo o mais
que você precisar está disponível no nosso
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Agradecimentos</h2>

<p>
A equipe do instalador do Debian agradece a todo mundo que contribuiu para esta
versão.
</p>

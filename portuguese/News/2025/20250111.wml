#use wml::debian::translation-check translation="c0534ce78eaba4104b9c0e724765ee740ea24c41"
<define-tag pagetitle>Atualização Debian 12: 12.9 lançado</define-tag>
<define-tag release_date>2025-01-11</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian está feliz em anunciar a nona atualização de sua
versão estável (stable) do Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, além de pequenos ajustes para problemas mais sérios. Avisos de
segurança já foram publicados em separado e são referenciados quando
necessário.</p>

<p>Por favor, note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de jogar fora as antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>Aquelas pessoas que frequentemente instalam atualizações a partir de
security.debian.org não terão que atualizar muitos pacotes, e a maioria de tais
atualizações estão incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
apontando o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Correções gerais de bugs</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction allow-html-temp "Update for Thunderbird 128 compatibility">
<correction ansible-core "New upstream stable release; fix arbitrary code execution issue [CVE-2024-11079]; fix information disclosure issue [CVE-2024-8775]; fix file overwrite issue [CVE-2024-9902]; fix test failure">
<correction audiofile "Fix null pointer dereference issue [CVE-2019-13147]; fix information leak issue [CVE-2022-24599]">
<correction avahi "Fix denial of service issues [CVE-2023-38469 CVE-2023-38470 CVE-2023-38471 CVE-2023-38472 CVE-2023-38473]; fix browsing when invalid services are present">
<correction base-files "Update for the point release">
<correction bochs "Build BIOS images for i386 CPUs">
<correction cpuinfo "Make test failures during build non-fatal">
<correction criu "Dynamically handle different libc at runtime than compilation time">
<correction debian-installer "Increase Linux kernel ABI to 6.1.0-29; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-security-support "Update list of packages receiving limited support in bookworm">
<correction debootstrap "Do not pull in usr-is-merged in trixie/sid">
<correction dnsmasq "Fix denial of service issues [CVE-2023-50387 CVE-2023-50868]; set default maximum EDNS.0 UDP packet size to 1232 [CVE-2023-28450]">
<correction eas4tbsync "Update for Thunderbird 128 compatibility">
<correction espeak-ng "Fix dropping last byte of stdin input">
<correction geoclue-2.0 "Use beaconDB rather than the now retired Mozilla Location Service">
<correction glib2.0 "Fix buffer overflow when configured to use a SOCKS4a proxy with a very long username [CVE-2024-52533]">
<correction gnuchess "Fix arbitrary code execution issue [CVE-2021-30184]">
<correction grml-rescueboot "Update supported architectures from amd64/i386 to arm64/amd64">
<correction gsl "Fix buffer overflow calculating the quantile value [CVE-2020-35357]">
<correction gst-plugins-base1.0 "Don't try parsing extended header if not enough data is available (id3v2) [CVE-2024-47542]">
<correction gunicorn "Prevent HTTP request smuggling [CVE-2024-1135]">
<correction icinga2 "Prevent TLS certificate bypass [CVE-2024-49369]">
<correction intel-microcode "New upstream security release [CVE-2024-21853 CVE-2024-23918 CVE-2024-24968 CVE-2024-23984]">
<correction jinja2 "Prevent HTML attribute injection [CVE-2024-22195 CVE-2024-34064]">
<correction lemonldap-ng "Fix privilege escalation when adaptive auth levels used [CVE-2024-52946]; fix XSS in upgrade plugin [CVE-2024-52947]">
<correction libebml "Fix buffer overflow issue [CVE-2023-52339]">
<correction libpgjava "Fix SQL injection issue [CVE-2024-1597]">
<correction libsoup2.4 "Prevent HTTP request smuggling [CVE-2024-52530]; fix buffer overflow in soup_header_parse_param_list_strict [CVE-2024-52531]; fix DoS reading from WebSocket clients [CVE-2024-52532]">
<correction libxstream-java "Fix denial of service issue [CVE-2024-47072]">
<correction linux "New upstream release; bump ABI to 29">
<correction linux-signed-amd64 "New upstream release; bump ABI to 29">
<correction linux-signed-arm64 "New upstream release; bump ABI to 29">
<correction linux-signed-i386 "New upstream release; bump ABI to 29">
<correction live-boot "Attempt DHCP on all connected interfaces">
<correction llvm-toolchain-19 "New source package, to support builds of chromium">
<correction lxc "Fix null pointer dereference when using a shared rootfs">
<correction mailmindr "Update for Thunderbird 128 compatibility">
<correction nfs-utils "Fix referrals when --enable-junction=no">
<correction nvidia-graphics-drivers "New upstream stable release [CVE-2024-0126]">
<correction nvidia-open-gpu-kernel-modules "New upstream LTS release [CVE-2024-0126]">
<correction oar "Add missing dependency on libcgi-fast-perl; fix oar user creation on new installations; fix SVG functions with PHP 8">
<correction opensc "Fix data leak issue [CVE-2023-5992]; fix use-after-free issue [CVE-2024-1454]; fix missing initialisation issue [CVE-2024-45615]; fix various issues with APDU buffer handling [CVE-2024-45616]; fix missing or incorrect function return value checks [CVE-2024-45617 CVE-2024-45618]; fix <q>incorrect handling of length of buffers or files</q> issues [CVE-2024-45619 CVE-2024-45620]; fix arbitary code execution issue [CVE-2024-8443]">
<correction openssh "Always use internal mkdtemp implementation; fix gssapi-keyex declaration; add ssh-gssapi automated test; don't prefer host-bound public key signatures if there was no initial host key; make sntrup761x25519-sha512 key exchange algorithm available without the @openssh.com suffix too">
<correction pgtcl "Install library in default Tcl auto_path">
<correction poco "Fix integer overflow issue [CVE-2023-52389]">
<correction prometheus-node-exporter-collectors "Reinstate missing `apt_package_cache_timestamp_seconds` metrics; fix apt_upgrades_pending and apt_upgrades_held metrics; improve heuristic for apt update last run time">
<correction pypy3 "Fix email address parsing issue [CVE-2023-27043]; fix possible Server Side Request Forgery issue [CVE-2024-11168]; fix private IP address range parsing [CVE-2024-4032]; fix regular expression based Denial of Service issue [CVE-2024-6232]; fix header injection issue [CVE-2024-6923]; fix denial of service issue [CVE-2024-7592 CVE-2024-8088]; fix command injection issue [CVE-2024-9287]">
<correction python-asyncssh "Fix <q>rogue extension negotiation</q> issue [CVE-2023-46445]; fix <q>rogue session attack</q> issue [CVE-2023-46446]">
<correction python-tornado "Fix open redirect issue [CVE-2023-28370]; fix denial of service issue [CVE-2024-52804]">
<correction python-urllib3 "Fix possible information leak during cross-origin redirects [CVE-2023-43804]; fix <q>request body not stripped after redirect from 303 status changes request method to GET</q> [CVE-2023-45803]; fix <q>Proxy-Authorization request header isn't stripped during cross-origin redirects</q> [CVE-2024-37891]">
<correction python-werkzeug "Fix denial of service when file upload begins with CR or LF [CVE-2023-46136]; fix arbitrary code execution on developer's machine via the debugger [CVE-2024-34069]; fix denial of service when processing multipart/form-data requests [CVE-2024-49767]">
<correction python3.11 "Reject malformed addresses in email.parseaddr() [CVE-2023-27043]; encode newlines in headers in the email module [CVE-2024-6923]; fix quadratic complexity parsing cookies with backslashes [CVE-2024-7592]; fix venv activation scripts failure to quote paths [CVE-2024-9287]; fix improper validation of bracketed hosts in urllib functions [CVE-2024-11168]">
<correction qemu "New upstream bugfix release [CVE-2024-7409]; mark internal codegen helper symbols as hidden, fixing build failure on arm64">
<correction quicktext "Update for Thunderbird 128 compatibility">
<correction redis "Fix denial of service with malformed ACL selectors [CVE-2024-31227]; fix denial of service through unbound pattern matching [CVE-2024-31228]; fix stack overflow [CVE-202431449]">
<correction renderdoc "Fix integer overflows [CVE-2023-33863 CVE-2023-33864]; fix symlink attack vector [CVE-2023-33865]">
<correction ruby-doorkeeper "Prevent skipping of authorization steps [CVE-2023-34246]">
<correction setuptools "Fix remote code execution issue [CVE-2024-6345]">
<correction sqlparse "Fix regular expression-related denial of service issue [CVE-2023-30608]; fix denial of service issue [CVE-2024-4340]">
<correction srt "Fix dependencies for consumers of the -dev packages">
<correction systemd "New upstream stable release">
<correction tango "Make the property_* tables compatible with MariaDB 10.11 at install time; add autopkgtest">
<correction tbsync "Update for Thunderbird 128 compatibility">
<correction texlive-bin "Fix data loss when using discretionaries with priorities; fix heap buffer overflow [CVE-2024-25262]">
<correction tiff "Fix buffer overflow issues [CVE-2023-25433 CVE-2023-26966]; fix use-after-free issue [CVE-2023-26965]; fix null pointer dereference issue [CVE-2023-2908]; fix denial of service issues [CVE-2023-3618 CVE-2023-52356 CVE-2024-7006]">
<correction tzdata "New upstream release: improve historical data for some zones; confirm lack of leap second for 2024">
<correction ucf "Initialise variable subsequently passed to eval">
<correction util-linux "Fix wider mitigation for CVE-2024-28085">
<correction xsane "Add Recommends for firefox-esr as well as firefox">
<correction zfs-linux "Add missing symbols in libzfs4linux and libzpool5linux; fix dnode dirty test [CVE-2023-49298]; fix sharenfs IPv6 address parsing [CVE-2013-20001]; fixes related to NULL pointer, memory allocation, etc.">
<correction zookeeper "Fix information disclosure in persistent watchers handling [CVE-2024-23944]">
</table>


<h2>Atualizações de segurança</h2>

<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable).
A equipe de segurança já lançou um aviso para cada uma dessas atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2024 5801 firefox-esr>
<dsa 2024 5803 thunderbird>
<dsa 2024 5804 webkit2gtk>
<dsa 2024 5805 guix>
<dsa 2024 5806 libarchive>
<dsa 2024 5807 nss>
<dsa 2024 5808 ghostscript>
<dsa 2024 5809 symfony>
<dsa 2024 5810 chromium>
<dsa 2024 5811 mpg123>
<dsa 2024 5812 postgresql-15>
<dsa 2024 5813 symfony>
<dsa 2024 5814 thunderbird>
<dsa 2024 5815 needrestart>
<dsa 2024 5816 libmodule-scandeps-perl>
<dsa 2024 5817 chromium>
<dsa 2024 5818 linux-signed-amd64>
<dsa 2024 5818 linux-signed-arm64>
<dsa 2024 5818 linux-signed-i386>
<dsa 2024 5818 linux>
<dsa 2024 5819 php8.2>
<dsa 2024 5820 firefox-esr>
<dsa 2024 5821 thunderbird>
<dsa 2024 5822 simplesamlphp>
<dsa 2024 5823 webkit2gtk>
<dsa 2024 5824 chromium>
<dsa 2024 5825 ceph>
<dsa 2024 5826 smarty3>
<dsa 2024 5827 proftpd-dfsg>
<dsa 2024 5828 python-aiohttp>
<dsa 2024 5829 chromium>
<dsa 2024 5830 smarty4>
<dsa 2024 5831 gst-plugins-base1.0>
<dsa 2024 5832 gstreamer1.0>
<dsa 2024 5833 dpdk>
<dsa 2024 5835 webkit2gtk>
<dsa 2024 5837 fastnetmon>
<dsa 2024 5838 gst-plugins-good1.0>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos por circunstâncias fora de nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction criu "[armhf] Fails to build on arm64 host">
<correction tk-html3 "Unmaintained; security issues">

</table>


<h2>Instalador do Debian</h2>

<p>O instalador foi atualizado para incluir as correções incorporadas
na versão estável (stable) pela versão pontual.</p>


<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informações da versão estável (stable) (notas de lançamento, errata, etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional completamente livre Debian.</p>


<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com a equipe de
lançamento da versão estável (stable) em
&lt;debian-release@lists.debian.org&gt;.</p>
